package provider

import (
	"cppcloud"
	"testing"
)

func TestTCPPrvd(t *testing.T) {
	appAttr := make(map[string]interface{})
	appAttr["svrid"] = 207
	appAttr["svrname"] = "go-tcp-prvd"
	capp := cppcloud.CreateCppCloudApp("cppcloud.cn:4800", appAttr, 3)
	capp.Start()

	prvd := CreateTCPProvider(capp, "", 2044)

	prvd.Start()
	prvd.Join()
}
